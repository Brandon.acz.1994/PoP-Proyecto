/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BookDAO;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Brandon
 */
public class BookDAOIterator implements Iterator<Book> {
    List<Book> items;
    int position =0;
    
    public BookDAOIterator (List<Book> items){
    this.items= items;
    }

    @Override
    public boolean hasNext() {
        if(position >= items.size()){
        return false;
        } else {
            
        return true;
        }
    }

    @Override
    public Book next() {
      
      Book book = items.get(position);
      position = position +1;
     
      
      return book;
    }
    
}
