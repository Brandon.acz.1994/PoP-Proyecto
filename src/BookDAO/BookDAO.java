/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BookDAO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brandon
 */
public class BookDAO implements BookDAOInterface {
    
    //lista funcionando como base de datos
    private static BookDAO instance;
    static private List<Book> bookslist= new ArrayList();
    
    
    
    private BookDAO(){
    
    }
    
    public static BookDAO getInstance(){
    
        if(instance==null){
        instance = new BookDAO();
        }
    return instance;   
    }

    @Override
    public List<Book> getAllBooks() {
      return bookslist;
    }    
    
    @Override
    public List<Book> getNewBooks(){
        
    List<Book> templist= new ArrayList();
    
        BookDAOIterator iter = iterator();      
       while(iter.hasNext()){          
       Book b= iter.next();
        
        if(b.getStatus().equals("new"))
        {
            templist.add(b);}
        
       
       }
      return templist;  
    }
    
    @Override
    public List<Book> getSemiNewBooks(){
        
    List<Book> templist= new ArrayList();
    
        BookDAOIterator iter = iterator();
       while(iter.hasNext()){
       Book b= iter.next();
        
        if(b.getStatus().equals("semi"))
           templist.add(b);
       
       }
      return templist;  
    }
    
    @Override
    public List<Book> getOldBooks(){
        
    List<Book> templist= new ArrayList();
    
        BookDAOIterator iter = iterator();
       while(iter.hasNext()){
       Book b= iter.next();
        
        if(b.getStatus().equals("old"))
           templist.add(b);
       
       }
      return templist;  
    }

    @Override
    public Book getBook(int ID,String type) {
       for (Book temp: bookslist){
           
           if(temp.getISBN()==ID && temp.getStatus().equals(type)){
           return temp;
           }
           
       }
      System.out.println("no se encuentra el libro o no existe");
      return null;        
    }
   

    @Override
    public void addBook(Book book) {
        bookslist.add(book);
    }

    @Override
    public void deleteBook(Book book) {
        bookslist.remove(book);
    }
    
    @Override
    public BookDAOIterator iterator(){
    return new BookDAOIterator(bookslist);
    }
    
}
