/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResupplyAlarm;

import BookDAO.Book;
import java.util.ArrayList;

/**
 *
 * @author Brandon
 */
public class ResupplyAlarm implements AlarmInterface{
    
    private static ArrayList<IBookOutOfStock> observers= new ArrayList();
    private static ResupplyAlarm instance;
 
private ResupplyAlarm(){
    
    observers.add(ResupplyRegister.getInstance());
    
}

public static ResupplyAlarm getInstance(){
 if(instance==null){
        instance = new ResupplyAlarm();
        }
    return instance;   

} 

    @Override
    public void attach(IBookOutOfStock observer) {
       observers.add(observer);
    }

    @Override
    public void dettach(IBookOutOfStock observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Book book) {
        for(int i =0; i< observers.size(); i++){
            observers.get(i).update(book);
        }
    }
    
}
