/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResupplyAlarm;

import BookDAO.Book;

/**
 *
 * @author Brandon
 */
public interface AlarmInterface {
    
    public void attach(IBookOutOfStock observer);
    public void dettach(IBookOutOfStock observer);
    public void notifyObservers(Book book);
    
}
