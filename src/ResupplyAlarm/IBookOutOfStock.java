/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResupplyAlarm;

import BookDAO.Book;

/**
 *
 * @author Brandon
 */
public interface IBookOutOfStock {
    
    public void update(Book book);
    
}
