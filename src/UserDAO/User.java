/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserDAO;

import BookDAO.Book;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brandon
 */
public class User {
    
    private int ID;
    private String name;
    private String fatherlastname;
    private String motherlastname;
    private String Type;
    private final List<Book> borrowedbooks=new ArrayList();


    public User(Integer ID,String name, String fatherlastname, String motherlastname, String Type) {
        this.ID=ID;
        this.name = name;
        this.fatherlastname = fatherlastname;
        this.motherlastname = motherlastname;
        this.Type = Type;
    }



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherlastname() {
        return fatherlastname;
    }

    public void setFatherlastname(String fatherlastname) {
        this.fatherlastname = fatherlastname;
    }

    public String getMotherlastname() {
        return motherlastname;
    }

    public void setMotherlastname(String motherlastname) {
        this.motherlastname = motherlastname;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }
    
    
    public List<Book> getBorrowedbooks() {
        return borrowedbooks;
    }
    
    
    public void addBorrowedbook(Book borrowedbook) {
        borrowedbooks.add(borrowedbook);
    }
    
    public void removeBorrowedbook(Book borrowedbook){
     borrowedbooks.remove(borrowedbook);    
    }  
        
    @Override
    public String toString(){
    
        return this.ID+" | "+this.name+" | "+this.fatherlastname+" | "+this.motherlastname+" | "+this.Type;
    }


    
    
    
}
