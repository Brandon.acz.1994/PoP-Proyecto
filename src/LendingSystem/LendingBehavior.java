/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LendingSystem;

import BookDAO.Book;
import UserDAO.User;


/**
 *
 * @author Brandon
 */
public interface LendingBehavior {   
    
    
    public User LendingService(User loggeduser);

    public Book Lending(String bookname);

    public User RecivingService(User user);
    
    public Book Reciving(Book book);
  
    public void ChangeUserType(User user);

    
}
