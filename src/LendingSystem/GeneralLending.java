/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LendingSystem;

import BookDAO.Book;
import BookDAO.BookDAO;
import UserDAO.User;
import UserDAO.UserDAO;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Brandon
 */
public class GeneralLending implements LendingBehavior {

     
    
    @Override
    public User LendingService(User loggeduser) {
      System.out.println("escribe titulo o palabra a buscar");
        Scanner sn = new Scanner(System.in);
        String bookname = sn.nextLine();
        
        System.out.println("buscando libros que coinciden con: "+bookname);          
        
        Book book = Lending(bookname);
        
        loggeduser.addBorrowedbook(book);
        
        return loggeduser;
    }

    @Override
    public Book Lending(String bookname) {
     
      Book book = null;     
      
      for(Book b: BookDAO.getInstance().getOldBooks()){
          
          if(b.getTitle().toLowerCase().contains(bookname)&&b.getStock()!=0)
          {  
             book=b;             
             b.setStock(b.getStock()-1);             
             System.out.println("prestando una copia vieja del libro"+" "+b.toString());
             LendingSystem.BookStockCheck(b);
          }
       
    }
       for(Book b: BookDAO.getInstance().getSemiNewBooks()){
          
          if(b.getTitle().toLowerCase().contains(bookname)&&book==null&&b.getStock()!=0)
          {   
             book=b;
             b.setStock(b.getStock()-1);            
             System.out.println("prestando una copia seminueva del libro"+" "+b.toString());
             LendingSystem.BookStockCheck(b);  
          }
       
    } 
       
       for(Book b: BookDAO.getInstance().getNewBooks()){
          
          if(b.getTitle().toLowerCase().contains(bookname)&&book==null&&b.getStock()!=0)
          {  
             book=b;
             b.setStock(b.getStock()-1);            
             System.out.println("prestando una copia nueva del libro"+" "+b.toString());
             LendingSystem.BookStockCheck(b);   
          }
       
    }   
      
      return book;
    
    }
    
    @Override
    public User RecivingService(User user) {
        List<Book> found = new ArrayList<Book>();
        for(Book b: user.getBorrowedbooks()){
        
            Book book = Reciving(b);
            found.add(book);
        }
        
        for(Book b:found){
        user.removeBorrowedbook(b);
        }
         
      
       return user; 
    }
    
    @Override
    public Book Reciving(Book book){
     for(Book b: BookDAO.getInstance().getAllBooks()){
          
          if(b.equals(book))
          {               
             b.setStock(b.getStock()+1);
             System.out.println("regresando una copia del libro"+" "+b.toString());
             return book;
          }
        
        } 
       return null; 
    }
     
    @Override
    public void ChangeUserType(User user) {
        
        System.out.println("seleccione el cambio de tipo de usuario de las siguientes opciones:");
        
          
        
        System.out.println("1. estudiante");
        System.out.println("2. profesor");
        System.out.println("3. no hacer nada");
        
        
        Scanner sn = new Scanner(System.in);
        
        try{
        int opcion = sn.nextInt();
        
        switch(opcion){
            case 1:
                UserDAO.getInstance().getUser(user.getID()).setType("student");
                 break;
            case 2:
                UserDAO.getInstance().getUser(user.getID()).setType("profesor");
                 break;
            case 3:
                
                 break;
                 
          default:
                   System.out.println("Solo números entre 1 y 2");
        
        }
        
        
         }catch (InputMismatchException e){
            System.out.println("ingrese solo numeros por favor");
            sn.next();
        }
        
       System.out.println("listo\n");
    }


    

   

}
