/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryexam;


import BookDAO.Book;
import BookDAO.BookDAO;
import LendingSystem.*;
import ResupplyAlarm.ResupplyRegister;
import UserDAO.User;
import UserDAO.UserDAO;
import java.io.IOException;
import java.nio.file.Files;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Brandon
 */
public class SystemMenu implements ISystemMenu{
    
    User loggeduser;
    LendingBehavior lendingbehavior;
   
    
    public SystemMenu(User loggeduser,LendingBehavior lendingbehavior){
        
      
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        
        while(!salir){
            
        this.loggeduser=UserDAO.getInstance().getUser(loggeduser.getID());
        this.lendingbehavior=LendingSystem.createBehavior(loggeduser);  
            
       System.out.println("-----------sistema bibliotecario---------------");       
       System.out.println("bienvenido usuario "+loggeduser.getName()+" "+loggeduser.getFatherlastname()+" "+loggeduser.getMotherlastname());
       System.out.println("su numero de cuenta es: "+loggeduser.getID());  
       System.out.println("usted tiene cuenta tipo: "+loggeduser.getType());
       System.out.println();       
            
        
        System.out.println("seleccione opcion del sistema:");
        
        System.out.println("1. Inicializar");
        System.out.println("2. Salir");
        System.out.println("3. Configuracion del sistema");
        System.out.println("4. Cambiar tipo de usuario");
        System.out.println("5. pedir prestamo de libros");
        System.out.println("6. Regreso de libros prestados");
        System.out.println("7. Mostrar contenido de la base de datos");
        System.out.println("8. Mostar lista de libros pendientes a resurtir");
        
        try{
        int opcion = sn.nextInt();
        System.out.println();
        
        
        switch(opcion){
            case 1:
                Inicialize();
                break;
            case 2:
                Exit();
                break;
            case 3:
                ConfigSystem();
                break;
            case 4:
                ChangeUserType();
                break;
            case 5:
                BorrowBook();
                break;
            case 6:
                ReturnBook();
                break;
            case 7:
                ShowDatabase();
                break;
            case 8:
                ShowResupplyNeeds();
                break;             
                
        default:
                   System.out.println("Solo números entre 1 y 8");
        
        }
        
        }catch (InputMismatchException e){
            System.out.println("ingrese solo numeros por favor");
            sn.next();
        }
        
    }
        
        
    }

      
    
    @Override
    public void Inicialize(){
     
        System.out.println("inicializando sistema");
        System.out.println("leyendo archivos de texto para convertirlos a objetos");        
        
        if(BookDAO.getInstance().getAllBooks().isEmpty()){
        try {
            List<String> booklist = Files.readAllLines(java.nio.file.Paths.get("test.txt"));

            for (String line: booklist){
                String[] tokens = line.split("-");
                Book book = new Book();

                book.setISBN(Integer.parseInt(tokens[0]));
                book.setTitle(tokens[1]);
                book.setAutor(tokens[2]);
                book.setEditorial(tokens[3]);
                book.setStatus(tokens[4]);
                book.setStock(Integer.parseInt(tokens[5]));
                
                BookDAO.getInstance().addBook(book);
            }

           
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
        
        
    
        System.out.println();
    }
    
    @Override
    public void Exit(){
        
        System.out.println("terminando operaciones del sistema");
        System.exit(0);
    
    }
    
    @Override
    public void ConfigSystem(){
        
     ResupplyRegister.getInstance().ResuplyBooks();
        
    }
    
    @Override
    public void ChangeUserType(){
        
        User user = UserDAO.getInstance().getUser(loggeduser.getID());
        lendingbehavior.ChangeUserType(user);
        
        System.out.println();
    }
    
    @Override
    public void BorrowBook(){        
      
        
        System.out.println("prestamo de libros");
        
        User user = lendingbehavior.LendingService(loggeduser);
        this.loggeduser=user;
        
        
       System.out.println(); 
    }
    
    @Override
    public void ReturnBook(){
        
        System.out.println("Regreso de libros"); 
        
        User user = lendingbehavior.RecivingService(loggeduser);        
        this.loggeduser=user;
       
       System.out.println();
    }
    
    @Override
    public void ShowDatabase(){
        System.out.println("----------Lista de usuarios----------");
        UserDAO.getInstance().getAllUsers().stream().forEach(p -> System.out.println(p.toString()));
        System.out.println();
        System.out.println("----------Catalogo de libros TODOS----------");
        BookDAO.getInstance().getAllBooks().stream().forEach(p -> System.out.println(p.toString()+" | #Stock: "+p.getStock()));
        System.out.println();
        System.out.println("----------Catalogo de libros NUEVOS----------");
        System.out.println();
        BookDAO.getInstance().getNewBooks().stream().forEach(p -> System.out.println(p.toString()+" | #Stock: "+p.getStock()));
        System.out.println();
        System.out.println("----------Catalogo de libros SEMINUEVOS----------");
        System.out.println();
        BookDAO.getInstance().getSemiNewBooks().stream().forEach(p -> System.out.println(p.toString()+" | #Stock: "+p.getStock()));
        System.out.println();
        System.out.println("----------Catalogo de libros VIEJOS----------");
        System.out.println();
        BookDAO.getInstance().getOldBooks().stream().forEach(p -> System.out.println(p.toString()+" | #Stock: "+p.getStock()));
        System.out.println();
            
    }
    
    @Override
    public void ShowResupplyNeeds(){
        System.out.println("----------Lista de libros que necesitan resurtirse----------");
        ResupplyRegister.getInstance().getLog().stream().forEach(p -> System.out.println(p.toString()));
        System.out.println();
        
    }
    
    
}
