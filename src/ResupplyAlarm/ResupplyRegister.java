/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResupplyAlarm;

import BookDAO.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brandon
 */
public class ResupplyRegister implements IBookOutOfStock{
    
    private static ResupplyRegister instance;
    static  private List<Book> log = new ArrayList();
    static  private List<Integer> ISBNlog = new ArrayList();
    
    
    private ResupplyRegister(){
    
    }

    public  List<Book> getLog() {
        return log;
    }
    
    public static ResupplyRegister getInstance(){
    
        if(instance==null){
        instance = new ResupplyRegister();
        }
    return instance;   
    }
    
    

    @Override
    public void update(Book book) {
       System.out.println("anexando libro a registro de espera de libros a resurtir ");
       
       
       
       if(!ISBNlog.contains(book.getISBN())){
           
       ISBNlog.add(book.getISBN());
       log.add(book);}
        
        
    }
    
    
    public void ResuplyBooks(){
    System.out.println("Realizando funcion para surtir libros\n");
      ResupplyRegister.log.stream().forEach(p -> {
      
          Book book = BookDAO.getInstance().getBook(p.getISBN(),"new");
          book.setStock(book.getStock()+5);
          System.out.println("libro "+p.toString()+" se le agrega 5 ejemplares mas");
      
      });
    log.removeAll(log);
    ISBNlog.removeAll(ISBNlog);
        
    System.out.println("Listo\n");
    }
    
}
