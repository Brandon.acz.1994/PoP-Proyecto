/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BookDAO;

import java.util.List;

/**
 *
 * @author Brandon
 */
public interface BookDAOInterface {
    
    public List<Book> getAllBooks();
    public List<Book> getNewBooks();
    public List<Book> getSemiNewBooks();
    public List<Book> getOldBooks();
    public Book getBook(int ID,String type);   
    public void addBook(Book book);
    public void deleteBook(Book book);
    public BookDAOIterator iterator();
    
}
