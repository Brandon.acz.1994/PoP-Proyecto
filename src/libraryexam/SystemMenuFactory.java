/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryexam;


import LendingSystem.LendingSystem;


import UserDAO.User;

/**
 *
 * @author Brandon
 */
public class SystemMenuFactory implements SystemMenuFactoryMethod {
    
    @Override
    public SystemMenu createMenu(User user){
        
    if(user.getType()=="student"){
      
    return new SystemMenu(user,LendingSystem.createBehavior(user));
    }
    if(user.getType()=="profesor"){
       
    return new SystemMenu(user,LendingSystem.createBehavior(user));
    }
    if(user.getType()=="general"){
        
    return new SystemMenu(user,LendingSystem.createBehavior(user));    
    }
    
    
    return null;    
    }
    
}
