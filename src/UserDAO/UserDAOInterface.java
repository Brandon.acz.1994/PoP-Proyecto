/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserDAO;


import java.util.List;

/**
 *
 * @author Brandon
 */
public interface UserDAOInterface {
    
    public List<User> getAllUsers();
    public User getUser(int ID);    
    public void addUser(User user);
    public void deleteUser(User user);
    
}
