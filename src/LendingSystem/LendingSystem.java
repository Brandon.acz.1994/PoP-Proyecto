/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LendingSystem;

import BookDAO.Book;
import ResupplyAlarm.ResupplyAlarm;
import UserDAO.User;


/**
 *
 * @author Brandon
 */
public class LendingSystem {
    
    
   static public LendingBehavior createBehavior(User user){
        
    if(user.getType()=="student"){
      
    return new StudentLending();
    }
    if(user.getType()=="profesor"){
       
    return new ProfesorLending();
    }
    if(user.getType()=="general"){
        
    return new GeneralLending();    
    }    
    
    return null;    
    }  
    


    static void BookStockCheck(Book b) {
     if(b.getStock()<=2){
     ResupplyAlarm a = ResupplyAlarm.getInstance();     
     
     a.notifyObservers(b);
     
     }
        
        
    }
    
}
