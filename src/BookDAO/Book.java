/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BookDAO;

/**
 *
 * @author Brandon
 */
public class Book {
    private int ISBN;
    private String title;
    private String autor;
    private String editorial;
    private String status;
    private int stock;
    
    public Book(int ISBN,String title,String status, int stock){
    this.ISBN=ISBN;
    this.title=title;    
    this.status=status;
    this.stock=stock;
        
    }

    public Book() {
        
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStock() {
           
        return stock;
    }

    public void setStock(int stock) {
    
        this.stock = stock;
        
        if(this.stock<=0){
        this.stock=0;
        }
    }
    
    @Override
    public String toString(){
    
    return this.ISBN+" | "+this.title+" | "+this.autor+" | "+this.editorial+" | "+this.status;
    }
    
    
}
