/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserDAO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brandon
 */
public class UserDAO implements UserDAOInterface {
    
    //lista funcionando como base de datos
    private static UserDAO instance;
    static private List<User> userlist = new ArrayList();
    
    private UserDAO(){
    
    }
    
    public static UserDAO getInstance(){
    if(instance==null){
        instance= new UserDAO();
    }
    return instance;
    }

    @Override
    public List<User> getAllUsers() {
       return userlist;
    }

    @Override
    public User getUser(int ID) {        
        for (User u:userlist){
            
            if (u.getID()==ID){
            return u;
            }
      
        }
        System.out.println("no se escuentra usuario con ID especificado");
        return null;
    }
    

    @Override
    public void addUser(User user) {
       userlist.add(user);
    }

    @Override
    public void deleteUser(User user) {
      userlist.remove(user);
    }

    
}
