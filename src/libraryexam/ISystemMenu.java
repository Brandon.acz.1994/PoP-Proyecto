/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryexam;

/**
 *
 * @author Brandon
 */
public interface ISystemMenu {
    
    public void Inicialize();
    public void Exit();
    public void ConfigSystem();
    public void ChangeUserType();
    public void BorrowBook();
    public void ReturnBook();
    public void ShowDatabase();
    public void ShowResupplyNeeds();
}
